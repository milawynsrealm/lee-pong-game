#include "game.h"

#define CAPTION_SIZE 64
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
SDL_Window *window = NULL;
SDL_Renderer *gRenderer = NULL;

/* Object positions */
int player_y = 25;
int comp_y = 25;
int ball_x = 100;
int ball_y = 100;
int player_score = 0;
int comp_score = 0;

void update_title(void)
{
    char tmpStr[CAPTION_SIZE];

    /* If there is no window, just return */
    if (window == NULL)
        return;

    /* Get the title set up */
    snprintf(tmpStr, CAPTION_SIZE, "Lee's Simple Pong Game ( %d | %d )", player_score, comp_score);

    /* Update the title of the window */
    SDL_SetWindowTitle(window, tmpStr);
}

int init_game(void)
{
    /* Initialize SDL before it can be used */
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Unable to initialize SDL library (Error: %s)\n", SDL_GetError());
        return 1;
    }

    /* Create the main window */
    window = SDL_CreateWindow("Lee's Simple Pong Game ( 0 | 0 )", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Unable to create a window (%dx%d - Error: %s)", SCREEN_WIDTH, SCREEN_HEIGHT, SDL_GetError());
        return 1;
    }

    /* Create the render surface to paint to */
    gRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (gRenderer == NULL)
    {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Unable to create a renderer (Error: %s)", SDL_GetError());
        return 1;
    }

    return 0;
}

void cleanup_game(void)
{
    /* Close the window */
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(window);
    window = NULL;
    gRenderer = NULL;

    /* Clear up SDL before exiting the game */
    SDL_Quit();
}

int process_events(void)
{
    SDL_Event event;

    while(SDL_PollEvent(&event) != 0)
    {
        /* ONLY return 1 if exiting the game */
        if (event.type == SDL_QUIT)
            return 1;

        /* If the player presses any key */
        if (event.type == SDL_KEYDOWN)
        {
            /* Determine which key was pressed */
            switch(event.key.keysym.sym)
            {
                case SDLK_w: /* Player 1 Up */
                {
                    if (player_y >= 25)
                        player_y -= 10;

                    break;
                }
                case SDLK_s: /* Player 1 Down */
                {
                    if (player_y <= (SCREEN_HEIGHT - 150))
                        player_y += 10;

                    break;
                }
                case SDLK_UP: /* Player 2 Up */
                {
                    if (comp_y >= 25)
                        comp_y -= 10;

                    break;
                }
                case SDLK_DOWN: /* Player 2 Down */
                {
                    if (comp_y <= (SCREEN_HEIGHT - 150))
                        comp_y += 10;
                }
                default:break;
            }
        }
    }

    return 0;
}

int draw(void)
{
    /* Clear the screen before redrawing */
    SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 0);

    if (SDL_RenderClear(gRenderer) != 0)
    {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Unable to clear the render surface (Error: %s)", SDL_GetError());
        return 1;
    }

    /* Draw the board pieces */
    DrawPaddle(gRenderer, 20, player_y);
    DrawPaddle(gRenderer, SCREEN_WIDTH - 52, comp_y);
    DrawBall(gRenderer, ball_x, ball_y);

    /* Update the screen */
    SDL_RenderPresent(gRenderer);

    return 0;
}

int process_logic(void)
{
    /* Make sure the scores don't go below 0 */
    if (player_score < 0)
        player_score = 0;

    if (comp_score < 0)
        comp_score = 0;

    /* Update the title */
    update_title();

    return 0;
}

int main(int argc, char *argv[])
{
    int quit = 0;

    /* Initialize the game */
    if(init_game() == 1)
        return 0;

    /* Main program loop */
    while(quit == 0)
    {
        /* Process input events from the keyboard */
        if (process_events() == 1)
            quit = 1;

        /* Draw everything */
        if (draw() == 1)
            quit = 1;

        /* Figure out what to do in certain scenarios */
        if (process_logic() == 1)
            quit = 1;
    }

    /* Close everything up before closing the game */
    cleanup_game();

    return 0;
}
