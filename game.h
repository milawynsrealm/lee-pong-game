#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <stdlib.h>

int DrawPaddle(SDL_Renderer *src, int x, int y);
int DrawBall(SDL_Renderer *src, int x, int y);
int DrawText(SDL_Renderer *src, int x, int y);

#endif /* GAME_H */
