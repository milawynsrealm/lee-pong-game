#include "game.h"

int _drawBox(SDL_Renderer *src, SDL_Rect paddle)
{
    int retVal = 0;

    /* Set the color of the paddle */
    retVal = SDL_SetRenderDrawColor(src, 255, 255, 255, 0);
    if (retVal != 0)
        return retVal;

    return SDL_RenderFillRect(src, &paddle);
}

int DrawPaddle(SDL_Renderer *src, int x, int y)
{
    SDL_Rect paddle;

    paddle.x = x;
    paddle.y = y;
    paddle.w = 32;
    paddle.h = 128;

    return _drawBox(src, paddle);
}

int DrawBall(SDL_Renderer *src, int x, int y)
{
    SDL_Rect paddle;

    paddle.x = x;
    paddle.y = y;
    paddle.w = 16;
    paddle.h = 16;

    return _drawBox(src, paddle);
}

int DrawText(SDL_Renderer *src, int x, int y)
{
    return 0;
}
